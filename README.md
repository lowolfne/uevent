UEvent
============

UEvent is a C# event system use for Unity3D

> In order to generate events,
> open `Window -> Tango -> UEvent` and it will open the **UEvent Generator** window.
>

Instructions:
  * Import the `UEvent.dll` and `UEventGenerator.cs` in your project
  ~~~
  UEvent.dll
  UEventGenerator.cs
  ~~~
  * Initialise the `UEvent` in `Awake` method before you start adding event listeners. Also, make sure to import the `using Tango;` in your script.
  ~~~cs
  using Tango;

  private void Awake()
  {
        UEventHud.Initialise< UGeneratedEventID >();
  }
  ~~~
  *  To add an `UEvent`, use the `UEventHub.AddListener( $eventName, $function )`
  ~~~cs
  private void Start()
  {
        UEventHub.AddListener( UGeneratedEventID.CREATE_EVENT, CreateEvent );
  }
  ~~~
  > Make sure the methods have the `IEventContext` as their parameter.
  ~~~cs
  public void CreateEvent( IEventContext context )
  {
        ...
  }
  ~~~
  * To trigger an `UEvent`, use the `UEventHub.PostEvent( $eventName )`
  ~~~cs
  private void PostEvent()
  {
        UEventHub.PostEvent( UGeneratedEventID.CREATE_EVENT );
  }
  ~~~
  * To remove an `UEvent`, use the `UEventHub.RemoveEvent( $eventName )`
  ~~~cs
  private void RemoveEvent()
  {
        UEventHub.RemoveEvent( UGeneratedEventID.CREATE_EVENT );
  }
  ~~~

  Todo
  ============
    - [x] Create UEvent editor for generating events
    - [ ] Handle scheduled events with specified `DateTime`
    - [ ] Create repository for the UEvent library code
