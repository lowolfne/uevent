﻿#define VERBOSE
using UnityEngine;
using Tango;

namespace Sample
{
	// a context
	public class GameContext : IEventContext
	{
		public int Count;
	}
	// NOTE: to test this, just assign to this to a gameobject
	public class Test : MonoBehaviour
	{
		private void Start()
		{
			GameContext gameContext = new GameContext()
			{
				Count = 10
			};
			
			// initial the UEvents
			UEventHub.Initialise<UGeneratedEventID>();
			
			// add listeners
			UEventHub.AddListener( UGeneratedEventID.PLAYER_AUTO_SAVE, OnPlayerAutoSave );
			UEventHub.AddListener( UGeneratedEventID.PLAYER_START, OnPlayerStart );
			UEventHub.AddListener( UGeneratedEventID.PLAYER_AUTO_REMIND, OnPlayerAutoRemind );
			UEventHub.AddListener( UGeneratedEventID.ENEMY_START, OnEnemyStart );
			
			// sets a context to a specified event
			UEventHub.TrySetEventContext( UGeneratedEventID.PLAYER_AUTO_SAVE, gameContext );
		}
		
		private void Update()
		{
			UEventHub.Loop( Time.deltaTime );
			
			if ( Input.GetKeyDown( KeyCode.A ) )
			{
				// trigger an event
				UEventHub.PostEvent( UGeneratedEventID.PLAYER_START );
			}
			
			if ( Input.GetKeyDown( KeyCode.S ) )
			{
				// trigger an event
				UEventHub.PostEvent( UGeneratedEventID.ENEMY_START );
			}
			
			if ( Input.GetKeyDown( KeyCode.D ) )
			{
				// trigger an event with context
				UEventHub.PostEvent( UGeneratedEventID.PLAYER_START, new GameContext()
				{
					Count = Random.Range( 90, 100 )
				} );
			}
			
			if ( Input.GetKeyDown( KeyCode.Return ) )
			{
				// stops a scheduled event
				UEventHub.TryStopScheduledEvent( UGeneratedEventID.PLAYER_AUTO_REMIND );
			}
		}
		
		private void OnPlayerStart( IEventContext context )
		{
			// get event id
			
			UEvent uEvent;
			UEventHub.TryGetEvent( UGeneratedEventID.PLAYER_START, out uEvent );
			
			// get assigned context, see line 32
			
			GameContext gameContext = context as GameContext;
			
			if ( gameContext != null && uEvent != null )
			{
#if VERBOSE
				UnityEngine.Debug.Log( "id: " + uEvent.Id + "Context " + gameContext.Count );
#endif
			}
		}
		
		private void OnPlayerAutoSave( IEventContext context )
		{
#if VERBOSE
			UnityEngine.Debug.Log( "OnPlayerAutoSave " );
#endif
			// removes a specified event
			UEventHub.RemoveListener( UGeneratedEventID.PLAYER_AUTO_SAVE, OnPlayerAutoSave );
		}
		
		private void OnPlayerAutoRemind( IEventContext context )
		{
#if VERBOSE
			UnityEngine.Debug.Log( " PLAYER_AUTO_REMIND " );
#endif
		}
		
		private void OnEnemyStart( IEventContext context )
		{
#if VERBOSE
			UnityEngine.Debug.Log( " ENEMY_START " );
#endif
		}
	}
}