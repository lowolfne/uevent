//------------------------------------------------
// Auto-generated do not edit
//
//
// Use the Window/Tango/Generate Events...
//
// Last Updated: 1/19/2019 11:01:11 PM
//------------------------------------------------
using Tango;
public partial class UGeneratedEventID
{
	public const string PLAYER_START = "PLAYER_START";
	private UEvent PLAYER_START_INIT = new UEvent( PLAYER_START ); //
	
	public const string PLAYER_EXIT = "PLAYER_EXIT";
	private UEvent PLAYER_EXIT_INIT = new UEvent( PLAYER_EXIT ); //
	
	public const string PLAYER_AUTO_SAVE = "PLAYER_AUTO_SAVE";
	private UEvent PLAYER_AUTO_SAVE_INIT = new UEvent( PLAYER_AUTO_SAVE, 5, true ); //Auto saves
	
	public const string PLAYER_AUTO_REMIND = "PLAYER_AUTO_REMIND";
	private UEvent PLAYER_AUTO_REMIND_INIT = new UEvent( PLAYER_AUTO_REMIND, 3, true ); //Reminds the player
	
}