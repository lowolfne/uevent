﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine;

public class PopupController : MonoBehaviour, IDragHandler
{
	[SerializeField]
	private Button _closeButton = null;
	
	[SerializeField]
	private Button _okButton = null;
	
	private void Start()
	{
		if ( _closeButton != null )
		{
			_closeButton.onClick.AddListener( OnClickClose );
		}
		
		if ( _okButton != null )
		{
			_okButton.onClick.AddListener( OnClickClose );
		}
	}
	
	private void OnClickClose()
	{
		Destroy( gameObject );
	}
	
	public void OnDrag( PointerEventData eventData )
	{
		transform.position += ( Vector3 )eventData.delta;
	}
}
