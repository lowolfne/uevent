﻿using UnityEngine;
using Tango;

public class ReminderController : MonoBehaviour
{
	[SerializeField]
	private float _popupTime = 3f;
	
	[SerializeField]
	private Animator _animator = null;
	
	private float _currentTime = 0f;
	private bool _shouldCountDown = false;
	
	private void Start()
	{
		//UnityEngine.Debug.Log("Start");
		UEventHub.AddListener( UGeneratedEventID.SCHEDULED_EVENT, OnAutoRemind );
	}
	
	// Update is called once per frame
	private void Update()
	{
		if ( _shouldCountDown )
		{
			_currentTime -= Time.deltaTime;
			
			if ( _currentTime <= 0 )
			{
				HideReminder();
			}
		}
	}
	
	private void OnAutoRemind( IEventContext context )
	{
		UnityEngine.Debug.Log( "Auto Remind!" );
		
		// trigger the auto remind open
		if ( _animator != null )
		{
			_animator.SetBool( "IsOpen", true );
		}
		
		// set popup time
		_currentTime = _popupTime;
	}
	
	public void StartCountDown()
	{
		_shouldCountDown = true;
	}
	
	private void HideReminder()
	{
		_shouldCountDown = false;
		
		// trigger the auto remind hide
		if ( _animator != null )
		{
			_animator.SetBool( "IsOpen", false );
		}
	}
}
