﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class TimeController : MonoBehaviour
{
	[SerializeField]
	private Text _timerText = null;
	
	// Update is called once per frame
	private void Update()
	{
		if ( _timerText )
		{
			_timerText.text = DateTime.Now.ToString( "hh:mm:ss tt" );
		}
	}
}
