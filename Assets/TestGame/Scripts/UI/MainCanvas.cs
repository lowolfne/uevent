﻿using UnityEngine;
using UnityEngine.UI;
using Tango;

public class MainCanvas : MonoBehaviour
{
	[SerializeField]
	private Button _createEventButton = null;
	
	[SerializeField]
	private Button _quitButton = null;
	
	private void Start()
	{
		if ( _createEventButton != null )
		{
			_createEventButton.onClick.AddListener( OnClickCreateEvent );
		}
		
		if ( _quitButton != null )
		{
			_quitButton.onClick.AddListener( OnClickQuit );
		}
	}
	
	private void OnClickCreateEvent()
	{
		UEventHub.PostEvent( UGeneratedEventID.CREATE_EVENT );
	}
	
	private void OnClickQuit()
	{
#if UNITY_EDITOR
		UnityEditor.EditorApplication.isPlaying = false;
#else
		Application.Quit();
#endif
	}
}
