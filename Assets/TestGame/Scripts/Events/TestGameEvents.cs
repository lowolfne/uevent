//------------------------------------------------
// Auto-generated do not edit
//
//
// Use the Window/Tango/Generate Events...
//
// Last Updated: 1/20/2019 4:25:35 PM
//------------------------------------------------
using Tango;
public partial class UGeneratedEventID
{
	public const string CREATE_EVENT = "CREATE_EVENT";
	private UEvent CREATE_EVENT_INIT = new UEvent( CREATE_EVENT, 0, false ); //
	
	public const string SCHEDULED_EVENT = "SCHEDULED_EVENT";
	private UEvent SCHEDULED_EVENT_INIT = new UEvent( SCHEDULED_EVENT, 5, true ); //Repeats every 5 seconds
	
}