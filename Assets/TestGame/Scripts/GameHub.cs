﻿#define VERBOSE
using UnityEngine;
using Tango;

public class GameHub : MonoBehaviour
{
	[SerializeField]
	private Canvas _mainCanvas = null;
	
	[SerializeField]
	private PopupController _popup = null;
	
	private static GameHub _instance;
	public static GameHub Instance
	{
		get
		{
			return _instance;
		}
	}
	
	private void Awake()
	{
		if ( _instance == null )
		{
			_instance = this;
		}
		else if ( _instance != this )
		{
			Destroy( gameObject );
		}
		
		UEventHub.Initialise< UGeneratedEventID >();
	}
	
	private void Start()
	{
		UEventHub.AddListener( UGeneratedEventID.CREATE_EVENT, CreateEvent );
		//UEventHub.AddListener( UGeneratedEventID.SCHEDULED_EVENT, PostScheduledEvent );
	}
	
	private void Update()
	{
		UEventHub.Loop( Time.deltaTime );
	}
	
	public void CreateEvent( IEventContext context )
	{
	
		if ( _popup != null )
		{
			PopupController controller = Instantiate( _popup, _mainCanvas.transform );
			controller.transform.position = new Vector2( Random.Range( 0, ( Screen.width / 2 ) ), Random.Range( 0, Screen.height / 2 ) );
		}
	}
	
	public void PostScheduledEvent( IEventContext context )
	{
	}
	
	private void OnApplicationQuit()
	{
		UEventHub.RemoveListener( UGeneratedEventID.CREATE_EVENT, CreateEvent );
		UEventHub.RemoveListener( UGeneratedEventID.SCHEDULED_EVENT, PostScheduledEvent );
		
		// or we can just call
		//UEventHub.Clear();
	}
}
